#####################     SIOMA S.A.S    ############################
#                Codigo de inicializacion de nukak
#####################################################################
######               Importacion de librerias                 #######
import json, requests
import os
import urllib.request, urllib.parse, urllib.error
import subprocess
import time
import sys
sys.path.insert(0, '/firmware/base/Codes/nukak')
######                        Funciones                       #######

######       Funcion para realizar la conexion a la web       #######
def Web_conex(tabla='estomas', datos='', timeout=6, intentos=1, tipo="GET"):
    SiomappCredentials = "C0l0mb14S10m4"
    mydata = [('s', SiomappCredentials), ('m', tabla), ('d', datos)]
    mydata = urllib.parse.urlencode(mydata)
    header = {"Content-type": "application/x-www-form-urlencoded"}
    if tipo == "GET":
        URL = 'http://filtro.siomapp.com/getBasura/56'
    elif tipo == "POST":
        URL = 'https://banapeso.siomapp.com/estomas/bajarTabla.php'
        campos = []
        campos.append("version")
        mydata = [('s', SiomappCredentials), ('serial', SERIAL), ('tabla', tabla), ('campos', json.dumps(campos))]
        mydata = urllib.parse.urlencode(mydata)
    else:
        print("ERROR: Ingrese un tipo de peticion valida ['GET', 'POST']")
        return None
    for i in range(0, intentos):
        i += 1
        try:
            if tipo == "GET":
                page = requests.get(URL)
                return page.content
            else:
                page = requests.post(URL, data=mydata, headers=header, timeout=timeout)
                return page.json()
        except Exception as e:
            print("ERROR EN LA CONEXION A SIOMA: " + repr(e))
            return 0


######      Funcion para la clonacion de un repo remoto       #######
def Clonar_repo(repo, repoWeb, emailSubject, emailBody, maxIntentosAct=1):
    intentos = 0
    descargar = 1
    while intentos < maxIntentosAct:
        ##### Se borra la carpeta local del repositorio
        try:
            borrar = os.system('sudo rm -R ' + repo)
        except Exception as e:
            msj = "No se borro el repositorio: "
            print(msj + repr(e))
            Web_noti(emailSubject, emailBody + msj)
            break
        print("borar: ", borrar)


        try:
            descargar = os.system(repoWeb)
        except Exception as e:
            msj = "No se descargo el repositorio: "
            print(msj + repr(e))
            Web_noti(emailSubject, emailBody + msj)
        print("descargar: ", descargar)

        if descargar != 0:
            return 1
        try:
            copiar = os.system('sudo cp ' + repo + ' -R /firmware/base/Codes')
        except Exception as e:
            msj = "No se copio el repositorio: "
            print(msj + repr(e))
            Web_noti(emailSubject, emailBody + msj)
        print("Copiar: ", copiar)

        intentos += 1

        if descargar == 0 and copiar == 0:
            return 0

    return 1


def Web_noti(subject, datos, timeout=6, intentos=1):
    print("Subject: ", subject)
    print("Datos a enviar: ", datos)
    SIOMAPPCREDENTIALS = "C0l0mb14S10m4"
    mydata = [('s', SIOMAPPCREDENTIALS), ('subject', subject), ('body', datos)]
    mydata = urllib.parse.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaStaff'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception as e:
                print("ERROR EN LA CONEXION A SIOMA: " + repr(e))
                return 0
        else:
            break

print("Iniciando validacion de version......")
######            Lectura de parametros locales               #######
f = open("/firmware/base/Codes/initbascula/info.txt", "r")
parametrosLocales = str.split(f.readline(), ',')
f.close()
SERIAL = parametrosLocales[0]
VERSION_LOCAL = float(parametrosLocales[2])
EQUIPO = parametrosLocales[1]
try:
    VERSION_LOCAL = VERSION_LOCAL.rstrip('\n')
except Exception as e:
    print("El parametro equipo no tiene el salto de linea")

print("Serial: ", SERIAL, " Version Local: ", VERSION_LOCAL, " Equipo: ", EQUIPO)

## Encendemos LCD ##
if EQUIPO == "nukak":
    import LCD
    import seg
    ### Inicializacion de LCD
    LCD.lcd_init()
    LCD.lcd_clean()
    LCD.lcd_fondo(100, 100, 100)
    LCD.lcd_string("Iniciando ....", 1,1)

######         Declaracion de variables y constantes          #######
succes = True
intentos = 0
MAXINTENTOSACTUALIZAR = 3
INTENTOSCONFIG = 3
CANTACTUALIZACIONES = 3

time.sleep(20)
print("Version local: ", VERSION_LOCAL)
try:
    version = float(Web_conex(tipo='POST')[0]['version'])
except:
    version = VERSION_LOCAL
print("Version remota: ", version)
if EQUIPO == "nukak":
    servicio = "bandejas"
    repo = "boot_nukak"
    repoWeb = "sudo git clone -b " + str(version) +\
              " --depth 1 https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/boot_nukak.git"
    copy = 'sudo cp boot_nukak -R /firmware/base/Codes'
    cmd = ['sudo', 'python3', '/firmware/base/Codes/boot_nukak/bootloader.py']
    run = 'sudo bash /firmware/base/Codes/nukak/InitParallel.sh'
elif EQUIPO == "sigma":
    servicio = "racimitos"
    repo = "boot_sigma"
    repoWeb = "sudo git clone -b " + str(version) + \
              " --depth 1 https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/boot_sigma.git"
    copy = 'sudo cp boot_sigma -R /firmware/base/Codes'
    cmd = ['sudo', 'python3', '/firmware/base/Codes/boot_sigma/bootloader.py']
    run = 'sudo bash /firmware/base/Codes/codes_sigma/InitParallel.sh'
else:
    sys.exit("ERROR: Tipo de equipo invalido")


if version > VERSION_LOCAL:
    if EQUIPO == "nukak":
        LCD.lcd_clean()
        LCD.lcd_fondo(100, 100, 100)
        LCD.lcd_string("Actualizando ...", 1, 1)
    print("Se va a actualizar")
    try:
        clone = Clonar_repo(repo, repoWeb, emailSubject="Error clonando bootloader",
                    emailBody="El equipo " + SERIAL + " tuvo inconvenientes clonando el bootloader.",
                    maxIntentosAct=MAXINTENTOSACTUALIZAR)

    except Exception as e:
        os.system(run)

    if clone == 0:
        print("Bootloader actualizado con exito")
        for i in range(0, INTENTOSCONFIG):
            bootloader = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            outs = bootloader.stdout.readlines()
            checks = []
            for line in outs:
                if line.decode('ascii').find('check:') > -1:
                    checks.append(line)

            valido = []
            for estado in checks:
                if estado.decode('ascii').find('True') > -1:
                    valido.append('ok')
                else:
                    break
            print(checks)
            if len(checks) == len(valido) == CANTACTUALIZACIONES:
                break

        if len(checks) == len(valido) == CANTACTUALIZACIONES:
            print("Bootloader ejecutado con exito")
            f = open("/firmware/base/Codes/initbascula/info.txt", "w")
            parametrosLocales = SERIAL + "," + str(EQUIPO) + "," + str(version)
            f.write(parametrosLocales)
            f.close()
            os.system('sudo reboot')
        else:
            if EQUIPO == "nukak":
                exit()
            else:
                os.system(run)
    else:
        if EQUIPO == "nukak":
            exit()
        else:
            os.system(run)

else:
    if EQUIPO == "nukak":
        exit()
    else:
        os.system(run)